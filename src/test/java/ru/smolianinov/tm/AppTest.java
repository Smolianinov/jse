package ru.smolianinov.tm;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import ru.smolianinov.tm.entity.Project;
import ru.smolianinov.tm.entity.Task;

/**
 * Unit test for simple App.
 */
public class AppTest {
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue() {

        final Application app = new Application();
        final Task task = app.getTaskService().findByIndex(1);
        System.out.println(task);
        final Project project = app.getProjectService().findByIndex(1);
        System.out.println(project);
        app.getProjectTaskService().addTaskToProject(project.getId(), task.getId());
        System.out.println(app.getProjectTaskService().findAddByProjectId(project.getId()));
        app.getProjectTaskService().removeTaskFromProject(project.getId(), task.getId());
        System.out.println(app.getProjectTaskService().findAddByProjectId(project.getId()));

        assertTrue(true);
    }
}
