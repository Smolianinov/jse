package ru.smolianinov.tm.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.smolianinov.tm.entity.CurrentUser;
import ru.smolianinov.tm.entity.Task;
import ru.smolianinov.tm.myExcept.TaskException;
import ru.smolianinov.tm.service.ProjectTaskService;
import ru.smolianinov.tm.service.TaskService;

import java.util.List;

public class TaskController extends AbstractController {

    private static final Logger logger = LogManager.getLogger(TaskController.class);

    private final TaskService taskService;

    private final ProjectTaskService projectTaskService;

    public TaskController(TaskService taskService, ProjectTaskService projectTaskService) {
        this.taskService = taskService;
        this.projectTaskService = projectTaskService;
    }

    public int createTask() {
        logger.info("[CREATE TASK]");
        logger.info("PLEASE, ENTER TASK NAME:");
        final String name = scanner.nextLine();
        logger.info("PLEASE, ENTER TASK DESCRIPTION:");
        final String description = scanner.nextLine();
        if (taskService.create(name, description) != null) {
            logger.info("[OK]");
        }
        return 0;
    }

    public int clearTask() {
        logger.info("[CLEAR TASK]");
        taskService.clear();
        logger.info("[OK]");
        return 0;
    }

    public void viewTask(final Task task) {
        if (task == null) return;
        logger.info("[VIEW TASK]");
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        logger.info("[OK]");
    }

    public int viewTaskByName() {
        logger.info("ENTER, TASK NAME:");
        final String name = scanner.nextLine();
        final Task task;
        try {
            task = taskService.findByName(name);
            viewTask(task);
        } catch (TaskException e) {
            e.printStackTrace();
        } finally {
            return 0;
        }

    }

    public int viewTaskByIndex() {
        logger.info("ENTER, TASK INDEX:");
        final int index = scanner.nextInt() - 1;
        final Task task = taskService.findByIndex(index);
        viewTask(task);
        return 0;
    }

    public int viewTaskById() {
        logger.info("ENTER, TASK ID:");
        final Long id = scanner.nextLong();
        final Task task = taskService.findById(id);
        viewTask(task);
        return 0;
    }

    public int listTask() {
        logger.info("[LIST TASK]");
        int index = 1;
        viewTask(taskService.findAll());
        logger.info("[OK]");
        return 0;
    }

    public void viewTask(List<Task> tasks) {
        if (tasks == null || tasks.isEmpty()) return;
        int index = 1;
        for (final Task task : tasks) {
            System.out.println(index + ". " + task.getId() + ": " + task.getName());
            index++;
        }
    }

    public int listTaskByProjectId() {
        logger.info("[LIST TASK BY PROJECT]");
        logger.info("PLEASE, ENTER PROJECT ID:");
        final long projectId = Long.parseLong(scanner.nextLine());
        final List<Task> tasks = taskService.findAddByProjectId(projectId);
        viewTask(tasks);
        logger.info("[OK]");
        return 0;
    }

    public int addTaskToProjectByIds() {
        logger.info("[ADD TASK TO PROJECT BY IDS]");
        logger.info("PLEASE, ENTER PROJECT ID:");
        final long projectId = Long.parseLong(scanner.nextLine());
        logger.info("[PLEASE, ENTER TASK ID]");
        final long taskId = Long.parseLong(scanner.nextLine());
        projectTaskService.addTaskToProject(projectId, taskId);
        logger.info("[OK]");
        return 0;
    }

    public int removeTaskToProjectByIds() {
        logger.info("[REMOVE TASK FROM PROJECT BY IDS]");
        logger.info("PLEASE, ENTER PROJECT ID:");
        final long projectId = Long.parseLong(scanner.nextLine());
        logger.info("[PLEASE, ENTER TASK ID]");
        final long taskId = Long.parseLong(scanner.nextLine());
        projectTaskService.removeTaskFromProject(projectId, taskId);
        logger.info("[OK]");
        return 0;
    }

    public int removeTaskByName() throws TaskException {
        logger.info("[REMOVE TASK BY NAME]");
        logger.info("PLEASE, ENTER TASK NAME:");
        final String name = scanner.nextLine();
        final Task task = taskService.removeByName(name);
        if (task == null) System.out.println("[FAIL]");
        else {
            logger.info("[OK]");
        }
        return 0;
    }

    public int removeTaskById() throws TaskException {
        logger.info("[REMOVE TASK BY ID]");
        logger.info("PLEASE, ENTER TASK ID:");
        final long id = scanner.nextLong();
        final Task task = taskService.removeById(id);
        if (task == null) {
            logger.info("[FAIL]");
        } else {
            logger.info("[OK]");
        }
        return 0;
    }

    public int removeTaskByIndex() {
        logger.info("[REMOVE TASK BY INDEX]");
        logger.info("PLEASE, ENTER TASK INDEX:");
        final int index = scanner.nextInt() - 1;
        final Task task = taskService.removeByIndex(index);
        if (task == null) {
            logger.info("[FAIL]");
        } else {
            logger.info("[OK]");
        }
        return 0;
    }

    public int updateTaskByIndex() {
        logger.info("[UPDATE TASK]");
        logger.info("ENTER TASK INDEX:");
        final int index = Integer.parseInt(scanner.nextLine()) - 1;
        final Task task = taskService.findByIndex(index);
        if (task == null) {
            logger.info("[FAIL]");
            return 0;
        }
        logger.info("[PLEASE, ENTER TASK NAME]");
        final String name = scanner.nextLine();
        logger.info("PLEASE, ENTER TASK DESCRIPTION:");
        final String description = scanner.nextLine();
        taskService.update(task.getId(), name, description);
        logger.info("[OK]");
        return 0;
    }

    public int updateTaskById() {
        logger.info("[UPDATE TASK]");
        logger.info("ENTER TASK ID:");
        final Long id = Long.parseLong(scanner.nextLine());
        final Task task = taskService.findById(id);
        if (task == null) {
            logger.info("[FAIL]");
            return 0;
        }
        logger.info("[PLEASE, ENTER TASK NAME]");
        final String name = scanner.nextLine();
        logger.info("PLEASE, ENTER TASK DESCRIPTION:");
        final String description = scanner.nextLine();
        taskService.update(task.getId(), name, description);
        logger.info("[OK]");
        return 0;
    }

    public int assignTaskToUser() {
        logger.info("ENTER, TASK ID:");
        final Long id = scanner.nextLong();
        if (id == null) {
            logger.info("[ID TASK CAN'T BE EMPTY]");
            return 0;
        }

        logger.info("ENTER, USER LOGIN:");
        final String username = scanner.next();
        if (username.isEmpty()) {
            logger.info("[LOGIN CAN'T BE EMPTY]");
            return 0;
        }

        Task task = taskService.assignTaskToUser(username, id);
        if (task == null) {
            logger.info("[FAIL]");
        } else {
            logger.info("[TASK WAS ADDED]");
        }
        return 0;
    }

    public int removeTaskFromUser() {
        logger.info("ENTER, TASK ID:");
        final Long id = scanner.nextLong();
        if (id == null) {
            logger.info("[ID TASK CAN'T BE EMPTY]");
        }

        taskService.removeTaskFromUser(id);
        logger.info("[TASK WAS REMOVED]");
        return 0;
    }

    public int viewTask() {
        final String currentUser = CurrentUser.getCurrentUser();
        if (currentUser == null || currentUser.isEmpty()) {
            logger.info("[AUTHENTICATE FIRST]");
            return 0;
        }
        if (currentUser.equals("ADMIN")) {
            logger.info("[LIST TASK]");
            int index = 1;
            for (final Task task : taskService.findAll()) {
                if (!task.getReponsibleUser().equals("")) {
                    System.out.println(index + ". " + task.getName() + " appointed for " + task.getReponsibleUser());
                }
                index++;
            }
        } else {
            logger.info("[LIST TASK]");
            int index = 1;
            for (final Task task : taskService.findAll()) {
                if (task.getReponsibleUser().equals(currentUser)) {
                    System.out.println(index + ". " + task.getName() + " appointed for " + task.getReponsibleUser());
                }
                index++;
            }
        }
        logger.info("[OK]");
        return 0;
    }

}
