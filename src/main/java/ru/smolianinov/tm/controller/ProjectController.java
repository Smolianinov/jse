package ru.smolianinov.tm.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.smolianinov.tm.entity.CurrentUser;
import ru.smolianinov.tm.entity.Project;
import ru.smolianinov.tm.myExcept.ProjectException;
import ru.smolianinov.tm.service.ProjectService;

public class ProjectController extends AbstractController {

    private static final Logger logger = LogManager.getLogger(ProjectController.class);

    private final ProjectService projectService;

    public ProjectController(ProjectService projectService) {
        this.projectService = projectService;
    }


    public int createProject() {
        logger.info("[CREATE PROJECT]");
        logger.info("PLEASE, ENTER PROJECT NAME:");
        final String name = scanner.nextLine();
        logger.info("PLEASE, ENTER PROJECT DESCRIPTION:");
        final String description = scanner.nextLine();
        if (projectService.create(name, description) != null) {
            logger.info("Created project: " + name);
        }
        return 0;
    }

    public int updateProjectByIndex() {
        logger.info("[UPDATE PROJECT]");
        logger.info("ENTER PROJECT INDEX:");
        final int index = Integer.parseInt(scanner.nextLine()) - 1;
        final Project project = projectService.findByIndex(index);
        if (project == null) {
            logger.info("[FAIL]");
            return 0;
        }
        logger.info("[PLEASE, ENTER PROJECT NAME]");
        final String name = scanner.nextLine();
        logger.info("PLEASE, ENTER PROJECT DESCRIPTION:");
        final String description = scanner.nextLine();
        projectService.update(project.getId(), name, description);
        logger.info("[OK]");
        return 0;
    }

    private static void viewProject(final Project project) {
        if (project == null) return;
        logger.info("[VIEW PROJECT]");
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        logger.info("[OK]");
    }

    public int updateProjectById() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final Long id = Long.parseLong(scanner.nextLine());
        final Project project = projectService.findById(id);
        if (project == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("[PLEASE, ENTER PROJECT NAME]");
        final String name = scanner.nextLine();
        System.out.println("PLEASE, ENTER PROJECT DESCRIPTION:");
        final String description = scanner.nextLine();
        projectService.update(project.getId(), name, description);
        logger.info("Updateded project, new name: " + name);
        return 0;
    }

    public int removeProjectByName() {
        System.out.println("[REMOVE PROJECT BY NAME]");
        System.out.println("PLEASE, ENTER PROJECT NAME:");
        final String name = scanner.nextLine();
        final Project project;
        try {
            project = projectService.removeByName(name);
            logger.info("Deleted project: " + name);
        } catch (ProjectException e) {
            logger.error(e);
        } finally {
            return 0;
        }
    }

    public int removeProjectById() throws ProjectException {
        logger.info("[REMOVE PROJECT BY ID]");
        logger.info("PLEASE, ENTER PROJECT ID:");
        final long id = scanner.nextLong();
        final Project project = projectService.removeById(id);
        if (project == null) System.out.println("[FAIL]");
        else {
            logger.info("[OK]");
        }
        return 0;
    }


    public int viewProjectByName() {
        logger.info("ENTER, PROJECT NAME:");
        final String name = scanner.nextLine();
        final Project project;
        try {
            project = projectService.findByName(name);
            viewProject(project);
        } catch (ProjectException e) {
            logger.error(e);
        } finally {
            return 0;
        }

    }

    public int viewProjectByIndex() {
        logger.info("ENTER, PROJECT INDEX:");
        final int index = scanner.nextInt() - 1;
        final Project project = projectService.findByIndex(index);
        viewProject(project);
        return 0;
    }

    public int viewProjectById() {
        logger.info("ENTER, PROJECT ID:");
        final Long id = scanner.nextLong();
        final Project project = projectService.findById(id);
        viewProject(project);
        return 0;
    }

    public int removeProjectByIndex() {
        logger.info("[REMOVE PROJECT BY INDEX]");
        logger.info("PLEASE, ENTER PROJECT INDEX:");
        final int index = scanner.nextInt() - 1;
        final Project project = projectService.removeByIndex(index);
        if (project == null) {
            logger.info("[FAIL]");
        } else {
            logger.info("[OK]");
        }
        return 0;
    }

    public int clearProject() {
        logger.info("[CLEAR PROJECT]");
        projectService.clear();
        logger.info("[OK]");
        return 0;
    }

    public int listProject() {
        logger.info("[LIST PROJECT]");
        int index = 1;
        for (final Project project : projectService.findAll()) {
            System.out.println(index + ". " + project.getId() + ": " + project.getName());
            index++;
        }
        logger.info("[OK]");
        return 0;
    }

    public int assignProjectToUser() {
        logger.info("ENTER, PROJECT ID:");
        final Long id = scanner.nextLong();
        if (id == null) {
            logger.info("[ID PROJECT CAN'T BE EMPTY]");
            return 0;
        }

        logger.info("ENTER, USER LOGIN:");
        final String username = scanner.next();
        if (username.isEmpty()) {
            logger.info("[LOGIN CAN'T BE EMPTY]");
            return 0;
        }

        Project project = projectService.assignProjectToUser(username, id);
        if (project == null) {
            logger.info("[FAIL]");
        } else {
            logger.info("[PROJECT WAS ADDED]");
            logger.trace("PROJECT: " + id + " WAS ADDED TO " + username);
        }

        return 0;
    }

    public int removeProjectFromUser() {
        logger.info("ENTER, PROJECT ID:");
        final Long id = scanner.nextLong();
        if (id == null) {
            logger.info("[ID PROJECT CAN'T BE EMPTY]");
        }

        projectService.removeProjectFromUser(id);
        logger.info("[PROJECT WAS REMOVED]");
        return 0;
    }

    public int viewProject() {
        final String currentUser = CurrentUser.getCurrentUser();
        if (currentUser == null || currentUser.isEmpty()) {
            logger.info("[AUTHENTICATE FIRST]");
            return 0;
        }
        if (currentUser.equals("ADMIN")) {
            logger.info("[LIST PROJECT]");
            int index = 1;
            for (final Project project : projectService.findAll()) {
                if (!project.getReponsibleUser().equals("")) {
                    System.out.println(index + ". " + project.getName() + " appointed for " + project.getReponsibleUser());
                }
                index++;
            }
        } else {
            logger.info("[LIST PROJECT]");
            int index = 1;
            for (final Project project : projectService.findAll()) {
                if (project.getReponsibleUser().equals(currentUser)) {
                    System.out.println(index + ". " + project.getName() + " appointed for " + project.getReponsibleUser());
                }
                index++;
            }
        }
        logger.info("[OK]");
        return 0;
    }

}
