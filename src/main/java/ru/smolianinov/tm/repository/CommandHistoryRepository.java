package ru.smolianinov.tm.repository;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.smolianinov.tm.controller.ProjectController;

import java.util.ArrayList;

public class CommandHistoryRepository {

    private static final Logger logger = LogManager.getLogger(CommandHistoryRepository.class);

    private ArrayList<String> history = new ArrayList<>();

    public void addCommandHistory(String command) {
        if (history.size() == 10) history.remove(0);
        history.add(command);
    }

    public int viewCommandHistory() {
        logger.info("[COMMAND HISTORY]");
        history.forEach((commands) -> System.out.println(commands));
        logger.info("[END COMMAND HISTORY]");
        return 0;
    }


}