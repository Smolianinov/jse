package ru.smolianinov.tm.repository;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.smolianinov.tm.entity.Task;
import ru.smolianinov.tm.myExcept.TaskException;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class TaskRepository {

    private static final Logger logger = LogManager.getLogger(TaskRepository.class);

    private final List<Task> tasks = new ArrayList<>();
    private final Map<String, Task> tasksMap = new LinkedHashMap<>();

    /**
     * Создать задачу
     *
     * @param name имя задачи
     * @return
     */
    public Task create(final String name) {
        final Task task = new Task(name);
        tasks.add(task);
        return task;
    }

    /**
     * Создать задачу
     *
     * @param name        имя задачи
     * @param description описание задачи
     * @return
     */
    public final Task create(final String name, String description) {

        if (tasksMap.containsKey(name)) {
            logger.info("Имя задачи уже существует. Задайте другое");
            return null;
        }

        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        tasks.add(task);
        tasksMap.put(name, task);
        return task;
    }

    /**
     * Изменить задачу
     *
     * @param id          номер задачи
     * @param name        имя задачи
     * @param description описание задачи
     * @return
     */
    public Task update(final Long id, final String name, final String description) {
        final Task task = findById(id);
        if (task == null) return null;
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    /**
     * Искать задачу по идентификатору
     *
     * @param index идентификатор задачи
     * @return
     */
    public Task findByIndex(final int index) {
        if (index < 0 || index > tasks.size() - 1) return null;
        return tasks.get(index);
    }

    /**
     * Искать задачу по имени
     *
     * @param name имя задачи
     * @return
     */
    public Task findByName(final String name)  throws TaskException {
        return tasksMap.get(name);
    }

    /**
     * Искать задачу по номеру
     *
     * @param id порядковый номер задачи
     * @return
     */
    public Task findById(final Long id) {
        if (id == null) return null;
        for (final Task task : tasks) {
            if (task.getId().equals(id)) return task;
        }
        return null;
    }


    public Task findByProjectIdAndId(final Long projectId, final Long id) {
        if (id == null) return null;
        for (final Task task : tasks) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (!idProject.equals(projectId)) continue;
            if (task.getId().equals(id)) return task;
        }
        return null;
    }

    /**
     * Удалить задачу по идентификатору
     *
     * @param id идентификатор заддачи
     * @return
     */
    public Task removeById(final Long id)  throws TaskException{
        final Task task = findById(id);
        if (task == null) {
            throw new TaskException("Задача с таким идентификатором не найден ");
        }
        tasks.remove(task);
        return task;
    }

    /**
     * Удалить задачу по номеру
     *
     * @param index порядковый номер задачи
     * @return
     */
    public Task removeByIndex(final int index) {
        final Task task = findByIndex(index);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    public List<Task> findAddByProjectId(final Long projectId) {
        List<Task> result = new ArrayList<>();
        for (final Task task : findAll()) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (idProject.equals(projectId)) result.add(task);
        }
        return result;
    }

    /**
     * Удалить задачу по имени
     *
     * @param name имя задачи
     * @return
     */
    public Task removeByName(final String name)  throws TaskException {
        final Task task = findByName(name);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    public void clear() {
        tasks.clear();
    }

    public List<Task> findAll() {
        return tasks;
    }

    public Task assignTaskToUser(final String login, final Long id) {
        final Task task = findById(id);
        if (task == null) {
            logger.info("[PROJECT ID NOT FOUND]");
            return null;
        }
        task.setReponsibleUser(login);
        return task;
    }

    public Task removeTaskFromUser(final Long id) {
        final Task task = findById(id);
        if (task == null) return null;
        task.setReponsibleUser("");
        return task;
    }

}
