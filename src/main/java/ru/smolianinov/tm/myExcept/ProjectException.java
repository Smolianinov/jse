package ru.smolianinov.tm.myExcept;

public class ProjectException extends Exception{

    public ProjectException(String message) {
        super(message);
    }
}
