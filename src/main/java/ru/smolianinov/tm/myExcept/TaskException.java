package ru.smolianinov.tm.myExcept;

public class TaskException extends Exception {

    public TaskException(String message) {
        super(message);
    }
}
